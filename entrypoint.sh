#!/bin/bash
set -eu
# 复制源码到CFS
echo "复制源码到CFS,可通过删除/var/www/html/uctoo/public/index.php文件进行覆盖安装"

if [ ! -e '/var/www/html/uctoo/public/index.php' ]; then
    cp -arf /var/www/uctoo /var/www/html/
fi
    chown -R www-data:www-data /var/www/html
    chmod -R 777 /var/www/html/

exec "$@"