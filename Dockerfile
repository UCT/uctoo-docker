FROM ubuntu:focal

LABEL Maintainer="patrick <contact@uctoo.com>" \
      Description="UCToo container with Nginx & PHP-FPM based on Ubuntu focal."

ARG DEBIAN_FRONTEND=noninteractive
ARG TZ=Asia/Shanghai

RUN apt-get update --no-install-recommends &&  \
    apt-get install -y --no-install-recommends tzdata apt-utils && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone && \
    apt-get install -y --no-install-recommends ca-certificates nano php-fpm nginx cron openssl php-mysql php-gd php-bcmath php-mbstring php-xml php-curl php-exif curl wget unzip git gettext && \
    rm -rf /var/lib/apt/lists/* && \
    sed -i '/session    required     pam_loginuid.so/c\#session    required   pam_loginuid.so' /etc/pam.d/cron

COPY root /
COPY ./entrypoint.sh /var/www/entrypoint.sh
RUN chown -R www-data:www-data /var/www && chmod +x /var/www/entrypoint.sh
WORKDIR /var/www
RUN git clone https://gitee.com/uctoo/uctoo.git
RUN chmod 777 -R /var/www && chmod 777 -R /usr/sbin && cd /var/www/uctoo

EXPOSE 80 443
ENTRYPOINT [ "/var/www/entrypoint.sh" ]
CMD ["/usr/sbin/supervisord", "-c", "/etc/supervisord.conf"]