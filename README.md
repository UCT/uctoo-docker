# uctoo-docker云开发部署

本项目基于开源项目 [CloudBase Framework](https://github.com/Tencent/cloudbase-framework) 开发部署，支持一键云端部署

后端应用容器（PHP）部署请点击
[![](https://main.qcloudimg.com/raw/67f5a389f1ac6f3b4d04c7256438e44f.svg)](https://console.cloud.tencent.com/tcb/env/index?action=CreateAndDeployCloudBaseProject&appUrl=https%3A%2F%2Fgitee.com%2FUCT%2Fuctoo-docker&branch=master)  
前端静态网站（VUE）部署请点击
[![](https://main.qcloudimg.com/raw/67f5a389f1ac6f3b4d04c7256438e44f.svg)](https://console.cloud.tencent.com/tcb/env/index?action=CreateAndDeployCloudBaseProject&appUrl=https%3A%2F%2Fgitee.com%2FUCT%2Fuctoo-app-server-vue&branch=master)
## 介绍

```shell
.
├── Dockerfile # 默认镜像声明文件
├── README.md
├── cloudbaserc.json # 云开发部署声明文件
├── docker-compose.yml
├── root
│   ├── etc
│   │   ├── nginx
│   │   ├── php
│   │   ├── supervisord.conf
│   │   └── supervisord_nginx.conf
│   ├── usr
│   │   └── sbin
│   │       └── supervisord
│   └── var
│       └── www
│           └── html
│             └── .env                # Thinkphp 6 配置文件
└── .env # docker 配置文件
```

## 下载代码

可通过以下开源项目进行二次开发。

1. [UCToo-docker项目源码](https://gitee.com/UCT/uctoo-docker)  https://gitee.com/UCT/uctoo-docker
2. [UCToo应用模板服务器端PHP项目](https://gitee.com/uctoo/uctoo)  https://gitee.com/uctoo/uctoo    [![](https://main.qcloudimg.com/raw/67f5a389f1ac6f3b4d04c7256438e44f.svg)](https://console.cloud.tencent.com/tcb/env/index?action=CreateAndDeployCloudBaseProject&appUrl=https%3A%2F%2Fgitee.com%2FUCT%2Fuctoo-docker&branch=master)  
3. [UCToo应用模板PC端VUE项目](https://gitee.com/UCT/uctoo-app-server-vue)  https://gitee.com/UCT/uctoo-app-server-vue    [![](https://main.qcloudimg.com/raw/67f5a389f1ac6f3b4d04c7256438e44f.svg)](https://console.cloud.tencent.com/tcb/env/index?action=CreateAndDeployCloudBaseProject&appUrl=https%3A%2F%2Fgitee.com%2FUCT%2Fuctoo-app-server-vue&branch=master)


### cloudbase 一键云端部署说明
1. 推荐使用cloudbase 一键部署。如安装时提示无本地环境，请先登录腾讯云->云开发cloudbase->环境总览开通一个云开发环境（目前只支持按量计费）。
2. 必须先一键部署UCToo应用模板服务器端PHP项目。项目部署完成后，在腾讯云->云开发cloudbase->我的应用，可以获取到后台访问地址，此地址即为后端baseAPI地址。
3. 通过腾讯云->云开发cloudbase->云托管->uctooserver服务->uctooserver-xxx版本->实例->Webshell登录后端实例（如果版本下没有实例，请先访问后端首页，激活一个实例运行），命令行 cd 至 /var/www/html/uctoo目录下，运行 php think uctoo:install 命令初始化数据库（如果安装命令运行失败，提示数据库休眠，请稍等几秒待数据库激活后再运行一遍命令。uctoo:install命令目前只支持cloudbase一键安装环境）。
4. 再一键部署UCToo应用模板PC端VUE项目。项目初始化部署时，用户需要填写以上步骤2获取到的baseAPI地址。前端VUE项目即可从baseAPI获取后端服务。部署完成后，在腾讯云->云开发cloudbase->我的应用，可以获取到前端访问地址。
5. 可通过腾讯云->云开发cloudbase->静态网站托管查看已部署的前端项目代码。
6. 可以通过腾讯云->云原生数据库TDSQL-C->登录，管理数据库。如果数据库未启动，请先启动。
7. 可以通过腾讯云->文件存储管理挂载的CFS文件系统。文件系统挂载于后端 /var/www/html/uctoo/public/uploads 目录，用于保存用户上传文件等内容。
8. 初始化安装后，cloudbase自动分配的前后端访问地址与用户自定义配置的前后端域名不一致，需在云托管->服务配置->HTTP访问服务配置（后端baseAPI地址），以及静态网站托管->基础配置->自定义域名配置（前端访问网址），用户设置的域名才可以生效。
9. 安装完成后，可用初始化安装设置的超管帐号登录管理后台，通过系统管理->模块管理功能安装更多内置模块，也可以到UCToo应用市场 https://appstore.uctoo.com 购买更多模块安装至本地实例。

### CI/CD 建议
1. 一键云端部署将git库的代码复制到了CFS持久化运行和保存变更。
2. 可以fork 源码库进行二次开发。修改Dockerfile中的git 地址部署二次开发版本。可通过删除CFS中的/var/www/html/uctoo/public/index.php文件进行覆盖安装。
3. 可在Webshell中 /var/www/html/uctoo/ 目录运行 php think uctoo:install -r 命令重置数据库内容至初始安装。
4. 产品内置API管理模块，可提供开发测试、持续迭代等基础特性。

### 云原生部署说明

```shell
# clone本项目至本地，配置 root/etc/nginx/conf.d/localhost.conf 修改为所需部署的域名，如需支持https，修改相应配置并将SSL证书复制到 ssl 目录下
server_name  www.uctoo.com;

# 配置 root/var/www/html/.env Thinkphp 6 配置文件，此配置文件在Dockerfile中通过以下命令部署到uctoo代码目录
COPY root/var/www/html/.env /var/www/html/uctoo/.env

# Dockerfile中通过以下命令clone uctoo源码加入image构建
RUN git clone https://gitee.com/uctoo/uctoo.git

# 配置本项目根目录.env文件用于docker构建和部署，云开发环境ID ENV_ID是在腾讯云开通cloudbase云开发环境时获得
ENV_ID=xxx

# 本项目通过supervisord启动了php-fpm和nginx两个服务
CMD ["/usr/sbin/supervisord", "-c", "/etc/supervisord.conf"]

# 将配置好参数的本项目打包成zip包，登录腾讯云通过云托管->新建服务->新建版本，选择本地代码上传方式，将本项目zip包上传，即可进行uctoo项目的构建和部署
# 本项目集成在www.uctoo.com ，是租户开通服务后，进行云原生应用部署的一个基础组件

```

## 二次开发

### 1、获取源码

clone uctoo源码 https://gitee.com/uctoo/uctoo.git
clone 前端项目源码 https://gitee.com/UCT/uctoo-app-server-vue ， 非cloudbase一键部署时，需打开.env配置文件中的 base api 配置项。

#### 方式1
1、可将二次开发后的源码复制到本项目 root/var/www/html 目录用于部署，请注意同时修改 nginx localhost.conf配置文件的代码目录

#### 方式2
2、可将二次开发后的uctoo源码提交git，修改 Dockerfile 中的 git clone 地址加入代码进行部署，请注意不要将.env等敏感数据提交到公开git，以免信息被盗
   ，Dockerfile 中git clone 私有仓库方式请自行解决。

### 2、构建/部署镜像

#### 步骤一

登录腾讯云，配置开发环境

#### 步骤二

参考云原生部署说明部分配置本项目参数

### 更新服务

### 方式1

登录云开发控制台，在云托管处点击[原版本编辑配置并重新部署](https://docs.cloudbase.net/run/update-service.html#fang-shi-er-yuan-ban-ben-bian-ji-pei-zhi-bing-chong-xin-bu-shu)。
通过上传本项目zip包方式进行部署

### 方式2

腾讯云控制台云托管->新建服务->新建版本，选择上传方式为代码库拉取，授权私有git库进行部署。

### 方式3

可在本地构建本项目的docker image，发布镜像到腾讯云镜像仓库或者hub.docker.com镜像仓库，在腾讯云控制台云托管->新建服务->新建版本，选择上传方式为镜像拉取进行部署
方式3在本地开发测试阶段经常采用，以下命令可参考
```shell
# 需在本地先安装docker、docker compose等工具
# 本项目根目录运行以下命令构建镜像
sudo docker-compose build --force-rm --no-cache
# 运行
sudo docker-compose up
# 停止
sudo docker-compose down
# 查看实例
sudo docker ps  
# 进入实例调试
sudo docker exec -it 49d4dfac7b27 /bin/bash
# 查看镜像
sudo docker images
# 镜像打标签
sudo docker tag uctoo:latest uctoo/uctoo:latest
# 登录docker hub 或登录腾讯云镜像库
sudo docker login
sudo docker login --username=xxx ccr.ccs.tencentyun.com
# 上传镜像，请注意不要将敏感数据上传至公开镜像，以免信息被盗
sudo docker image push uctoo/uctoo:latest

```

## FAQ

### 1、更新Thinkphp环境变量不生效

`root/var/www/html/.env` 配置文件在Dockerfile中未正确复制到代码目录中。可以通过 [webshell](https://docs.cloudbase.net/run/webshell.html#cao-zuo-bei-jing) 登录进行验证和修改。默认镜像内只有 nano 编辑器。

## Roadmap
1. 支持阿里云、华为云等符合云原生PaaS平台部署规范的公有云一键部署

## Tips
1. 如能采用云原生部署的项目建议尽可能采用云原生部署，不仅可保障部署环境的一致，云托管还可以降低部署和运维成本，资源按使用量精确计费。
2. 如果站点一直没有流量访问，云托管将于半小时内缩容到 0，如果有较大业务量，云托管可自动扩容。
3. 腾讯cloudbase文档显示，对Thinkphp框架仅支持 6.x以上版本，新项目建议尽早采用Thinkphp 6.x版本。